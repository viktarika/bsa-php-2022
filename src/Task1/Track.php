<?php

declare(strict_types=1);

namespace App\Task1;

class Track
{
    public array $addCar  = [];
    
    public function __construct(float $lapLength, int $lapsNumber)
    {
        $this->lapLength = $lapLength;
        $this->lapsNumber = $lapsNumber;
    }

    public function getLapLength(): float
    {
        return $this->lapLength;
    }

    public function getLapsNumber(): int
    {
        return $this->lapsNumber;
    }
    
    
    public function add(Car $car): void
    {
        array_push($this->addCar, $car);
    }

    public function all(): array
    {
        return $this->addCar;
    }

    public function run(): Car
    { 

          //get all cars 
        $allCar = $this->all(); 
        //all distanse 
        $allDistanse = $this->getLapLength() * $this->getLapsNumber(); 
        $arrTCar = [];
         foreach($allCar as $valCar){   
            //довжина, яку може проїхати car 
            $allDistanseCar = ($valCar->fuelTankVolume/$valCar->fuelConsumption)*100; 
            //к-ть pitStop, який вик-є car 
            $countPitStop = $allDistanse/$allDistanseCar;
            //час, який витрачає car на всі pitStop
            $tAllpitStop =  $countPitStop*$valCar->pitStopTime;
            //час, з якою car проїде трек 
            $tCar =  ($allDistanse/$valCar->speed)+$tAllpitStop; 
            //записуємо в массив id автом і її час прибуття
            $arrTCar[$valCar->id] = intval($tCar);
         }

        $minTCar = 0;
         foreach($arrTCar as $keyTCar => $valTCar){
            
           if ($valTCar < $minTCar) {
                $minTCar = $valTCar;
                $keyTCar = $keyTCar;
            }
         }

         foreach ($allCar as $k => $v) {
            if ($v->id == $keyTCar) {
                return $allCar[$k];
            }
        }
    }
    
    
}