<?php

declare(strict_types=1);

namespace App\Task2;

class BooksGenerator
{
    public function __construct(int $minPagesNumber, array $libraryBooks, int $maxPrice, array $storeBooks)
    {
        $this->minPagesNumber = $minPagesNumber;
        $this->libraryBooks = $libraryBooks;
        $this->maxPrice = $maxPrice;
        $this->storeBooks = $storeBooks;
    }

    public array $filteredBooks = [];
    public function generate(): \Generator
    {
              foreach ($this->libraryBooks as $valLibraryBooks) {
            if ($valLibraryBooks->getPagesNumber() >= $this->minPagesNumber) {
                $this->filteredBooks[] = new Book($valLibraryBooks->getTitle(), $valLibraryBooks->getPrice(), $valLibraryBooks->getPagesNumber());
            }
        }
        foreach ($this->storeBooks as $valStoreBooks) {
            if ($valStoreBooks->getPrice() <= $this->maxPrice) {
                $this->filteredBooks[] = new Book($valStoreBooks->getTitle(), $valStoreBooks->getPrice(), $valStoreBooks->getPagesNumber());
            }
        }
        foreach ($this->filteredBooks as $valfilteredBooks) {
            yield $valfilteredBooks;
        }
    }
}